gdjs.GamePlayCode = {};
gdjs.GamePlayCode.GDBallObjects1_1final = [];

gdjs.GamePlayCode.GDSpikeObjects1_1final = [];

gdjs.GamePlayCode.GDgameOverObjects1_1final = [];

gdjs.GamePlayCode.GDBallObjects1= [];
gdjs.GamePlayCode.GDBallObjects2= [];
gdjs.GamePlayCode.GDBallObjects3= [];
gdjs.GamePlayCode.GDBallObjects4= [];
gdjs.GamePlayCode.GDBallObjects5= [];
gdjs.GamePlayCode.GDBallObjects6= [];
gdjs.GamePlayCode.GDGroundObjects1= [];
gdjs.GamePlayCode.GDGroundObjects2= [];
gdjs.GamePlayCode.GDGroundObjects3= [];
gdjs.GamePlayCode.GDGroundObjects4= [];
gdjs.GamePlayCode.GDGroundObjects5= [];
gdjs.GamePlayCode.GDGroundObjects6= [];
gdjs.GamePlayCode.GDmoveGroundObjects1= [];
gdjs.GamePlayCode.GDmoveGroundObjects2= [];
gdjs.GamePlayCode.GDmoveGroundObjects3= [];
gdjs.GamePlayCode.GDmoveGroundObjects4= [];
gdjs.GamePlayCode.GDmoveGroundObjects5= [];
gdjs.GamePlayCode.GDmoveGroundObjects6= [];
gdjs.GamePlayCode.GDgameOverObjects1= [];
gdjs.GamePlayCode.GDgameOverObjects2= [];
gdjs.GamePlayCode.GDgameOverObjects3= [];
gdjs.GamePlayCode.GDgameOverObjects4= [];
gdjs.GamePlayCode.GDgameOverObjects5= [];
gdjs.GamePlayCode.GDgameOverObjects6= [];
gdjs.GamePlayCode.GDUIObjects1= [];
gdjs.GamePlayCode.GDUIObjects2= [];
gdjs.GamePlayCode.GDUIObjects3= [];
gdjs.GamePlayCode.GDUIObjects4= [];
gdjs.GamePlayCode.GDUIObjects5= [];
gdjs.GamePlayCode.GDUIObjects6= [];
gdjs.GamePlayCode.GDgroundSpawnObjects1= [];
gdjs.GamePlayCode.GDgroundSpawnObjects2= [];
gdjs.GamePlayCode.GDgroundSpawnObjects3= [];
gdjs.GamePlayCode.GDgroundSpawnObjects4= [];
gdjs.GamePlayCode.GDgroundSpawnObjects5= [];
gdjs.GamePlayCode.GDgroundSpawnObjects6= [];
gdjs.GamePlayCode.GDjumpSpawnObjects1= [];
gdjs.GamePlayCode.GDjumpSpawnObjects2= [];
gdjs.GamePlayCode.GDjumpSpawnObjects3= [];
gdjs.GamePlayCode.GDjumpSpawnObjects4= [];
gdjs.GamePlayCode.GDjumpSpawnObjects5= [];
gdjs.GamePlayCode.GDjumpSpawnObjects6= [];
gdjs.GamePlayCode.GDJumpObjects1= [];
gdjs.GamePlayCode.GDJumpObjects2= [];
gdjs.GamePlayCode.GDJumpObjects3= [];
gdjs.GamePlayCode.GDJumpObjects4= [];
gdjs.GamePlayCode.GDJumpObjects5= [];
gdjs.GamePlayCode.GDJumpObjects6= [];
gdjs.GamePlayCode.GDSpikeObjects1= [];
gdjs.GamePlayCode.GDSpikeObjects2= [];
gdjs.GamePlayCode.GDSpikeObjects3= [];
gdjs.GamePlayCode.GDSpikeObjects4= [];
gdjs.GamePlayCode.GDSpikeObjects5= [];
gdjs.GamePlayCode.GDSpikeObjects6= [];
gdjs.GamePlayCode.GDBarrierObjects1= [];
gdjs.GamePlayCode.GDBarrierObjects2= [];
gdjs.GamePlayCode.GDBarrierObjects3= [];
gdjs.GamePlayCode.GDBarrierObjects4= [];
gdjs.GamePlayCode.GDBarrierObjects5= [];
gdjs.GamePlayCode.GDBarrierObjects6= [];
gdjs.GamePlayCode.GDCountDownObjects1= [];
gdjs.GamePlayCode.GDCountDownObjects2= [];
gdjs.GamePlayCode.GDCountDownObjects3= [];
gdjs.GamePlayCode.GDCountDownObjects4= [];
gdjs.GamePlayCode.GDCountDownObjects5= [];
gdjs.GamePlayCode.GDCountDownObjects6= [];
gdjs.GamePlayCode.GDScoreObjects1= [];
gdjs.GamePlayCode.GDScoreObjects2= [];
gdjs.GamePlayCode.GDScoreObjects3= [];
gdjs.GamePlayCode.GDScoreObjects4= [];
gdjs.GamePlayCode.GDScoreObjects5= [];
gdjs.GamePlayCode.GDScoreObjects6= [];
gdjs.GamePlayCode.GDStarObjects1= [];
gdjs.GamePlayCode.GDStarObjects2= [];
gdjs.GamePlayCode.GDStarObjects3= [];
gdjs.GamePlayCode.GDStarObjects4= [];
gdjs.GamePlayCode.GDStarObjects5= [];
gdjs.GamePlayCode.GDStarObjects6= [];
gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects1= [];
gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects2= [];
gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects3= [];
gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects4= [];
gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects5= [];
gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects6= [];


gdjs.GamePlayCode.asyncCallback9802604 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(runtimeScene.getObjects("Barrier"), gdjs.GamePlayCode.GDBarrierObjects6);
gdjs.copyArray(asyncObjectsList.getObjects("CountDown"), gdjs.GamePlayCode.GDCountDownObjects6);

{for(var i = 0, len = gdjs.GamePlayCode.GDCountDownObjects6.length ;i < len;++i) {
    gdjs.GamePlayCode.GDCountDownObjects6[i].hide();
}
}{for(var i = 0, len = gdjs.GamePlayCode.GDBarrierObjects6.length ;i < len;++i) {
    gdjs.GamePlayCode.GDBarrierObjects6[i].deleteFromScene(runtimeScene);
}
}}
gdjs.GamePlayCode.eventsList0 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.GamePlayCode.GDCountDownObjects5) asyncObjectsList.addObject("CountDown", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.GamePlayCode.asyncCallback9802604(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.GamePlayCode.asyncCallback9802716 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("CountDown"), gdjs.GamePlayCode.GDCountDownObjects5);

{for(var i = 0, len = gdjs.GamePlayCode.GDCountDownObjects5.length ;i < len;++i) {
    gdjs.GamePlayCode.GDCountDownObjects5[i].setString("1");
}
}
{ //Subevents
gdjs.GamePlayCode.eventsList0(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.GamePlayCode.eventsList1 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.GamePlayCode.GDCountDownObjects4) asyncObjectsList.addObject("CountDown", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.GamePlayCode.asyncCallback9802716(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.GamePlayCode.asyncCallback9802404 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("CountDown"), gdjs.GamePlayCode.GDCountDownObjects4);

{for(var i = 0, len = gdjs.GamePlayCode.GDCountDownObjects4.length ;i < len;++i) {
    gdjs.GamePlayCode.GDCountDownObjects4[i].setString("2");
}
}
{ //Subevents
gdjs.GamePlayCode.eventsList1(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.GamePlayCode.eventsList2 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.GamePlayCode.GDCountDownObjects3) asyncObjectsList.addObject("CountDown", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.GamePlayCode.asyncCallback9802404(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.GamePlayCode.asyncCallback9802012 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("CountDown"), gdjs.GamePlayCode.GDCountDownObjects3);

{for(var i = 0, len = gdjs.GamePlayCode.GDCountDownObjects3.length ;i < len;++i) {
    gdjs.GamePlayCode.GDCountDownObjects3[i].setString("3");
}
}
{ //Subevents
gdjs.GamePlayCode.eventsList2(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.GamePlayCode.eventsList3 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.GamePlayCode.GDCountDownObjects2) asyncObjectsList.addObject("CountDown", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.GamePlayCode.asyncCallback9802012(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.GamePlayCode.asyncCallback9800516 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("CountDown"), gdjs.GamePlayCode.GDCountDownObjects2);

{for(var i = 0, len = gdjs.GamePlayCode.GDCountDownObjects2.length ;i < len;++i) {
    gdjs.GamePlayCode.GDCountDownObjects2[i].setString("4");
}
}
{ //Subevents
gdjs.GamePlayCode.eventsList3(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.GamePlayCode.eventsList4 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
for (const obj of gdjs.GamePlayCode.GDCountDownObjects1) asyncObjectsList.addObject("CountDown", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(1), (runtimeScene) => (gdjs.GamePlayCode.asyncCallback9800516(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.GamePlayCode.eventsList5 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Ball"), gdjs.GamePlayCode.GDBallObjects2);
{gdjs.evtTools.camera.centerCamera(runtimeScene, (gdjs.GamePlayCode.GDBallObjects2.length !== 0 ? gdjs.GamePlayCode.GDBallObjects2[0] : null), true, "", 0);
}{for(var i = 0, len = gdjs.GamePlayCode.GDBallObjects2.length ;i < len;++i) {
    gdjs.GamePlayCode.GDBallObjects2[i].rotate(400, runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Ball"), gdjs.GamePlayCode.GDBallObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.GamePlayCode.GDBallObjects1.length;i<l;++i) {
    if ( gdjs.GamePlayCode.GDBallObjects1[i].getX() > 1900 ) {
        isConditionTrue_0 = true;
        gdjs.GamePlayCode.GDBallObjects1[k] = gdjs.GamePlayCode.GDBallObjects1[i];
        ++k;
    }
}
gdjs.GamePlayCode.GDBallObjects1.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Score"), gdjs.GamePlayCode.GDScoreObjects1);
{for(var i = 0, len = gdjs.GamePlayCode.GDScoreObjects1.length ;i < len;++i) {
    gdjs.GamePlayCode.GDScoreObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("Score"))));
}
}}

}


};gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDBallObjects2Objects = Hashtable.newFrom({"Ball": gdjs.GamePlayCode.GDBallObjects2});
gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDgameOverObjects2Objects = Hashtable.newFrom({"gameOver": gdjs.GamePlayCode.GDgameOverObjects2});
gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDBallObjects2Objects = Hashtable.newFrom({"Ball": gdjs.GamePlayCode.GDBallObjects2});
gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDSpikeObjects2Objects = Hashtable.newFrom({"Spike": gdjs.GamePlayCode.GDSpikeObjects2});
gdjs.GamePlayCode.eventsList6 = function(runtimeScene) {

{

gdjs.GamePlayCode.GDBallObjects1.length = 0;

gdjs.GamePlayCode.GDSpikeObjects1.length = 0;

gdjs.GamePlayCode.GDgameOverObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.GamePlayCode.GDBallObjects1_1final.length = 0;
gdjs.GamePlayCode.GDSpikeObjects1_1final.length = 0;
gdjs.GamePlayCode.GDgameOverObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("Ball"), gdjs.GamePlayCode.GDBallObjects2);
gdjs.copyArray(runtimeScene.getObjects("gameOver"), gdjs.GamePlayCode.GDgameOverObjects2);
isConditionTrue_1 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDBallObjects2Objects, gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDgameOverObjects2Objects, false, runtimeScene, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.GamePlayCode.GDBallObjects2.length; j < jLen ; ++j) {
        if ( gdjs.GamePlayCode.GDBallObjects1_1final.indexOf(gdjs.GamePlayCode.GDBallObjects2[j]) === -1 )
            gdjs.GamePlayCode.GDBallObjects1_1final.push(gdjs.GamePlayCode.GDBallObjects2[j]);
    }
    for (let j = 0, jLen = gdjs.GamePlayCode.GDgameOverObjects2.length; j < jLen ; ++j) {
        if ( gdjs.GamePlayCode.GDgameOverObjects1_1final.indexOf(gdjs.GamePlayCode.GDgameOverObjects2[j]) === -1 )
            gdjs.GamePlayCode.GDgameOverObjects1_1final.push(gdjs.GamePlayCode.GDgameOverObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("Ball"), gdjs.GamePlayCode.GDBallObjects2);
gdjs.copyArray(runtimeScene.getObjects("Spike"), gdjs.GamePlayCode.GDSpikeObjects2);
isConditionTrue_1 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDBallObjects2Objects, gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDSpikeObjects2Objects, false, runtimeScene, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.GamePlayCode.GDBallObjects2.length; j < jLen ; ++j) {
        if ( gdjs.GamePlayCode.GDBallObjects1_1final.indexOf(gdjs.GamePlayCode.GDBallObjects2[j]) === -1 )
            gdjs.GamePlayCode.GDBallObjects1_1final.push(gdjs.GamePlayCode.GDBallObjects2[j]);
    }
    for (let j = 0, jLen = gdjs.GamePlayCode.GDSpikeObjects2.length; j < jLen ; ++j) {
        if ( gdjs.GamePlayCode.GDSpikeObjects1_1final.indexOf(gdjs.GamePlayCode.GDSpikeObjects2[j]) === -1 )
            gdjs.GamePlayCode.GDSpikeObjects1_1final.push(gdjs.GamePlayCode.GDSpikeObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.GamePlayCode.GDBallObjects1_1final, gdjs.GamePlayCode.GDBallObjects1);
gdjs.copyArray(gdjs.GamePlayCode.GDSpikeObjects1_1final, gdjs.GamePlayCode.GDSpikeObjects1);
gdjs.copyArray(gdjs.GamePlayCode.GDgameOverObjects1_1final, gdjs.GamePlayCode.GDgameOverObjects1);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.leaderboards.saveConnectedPlayerScore(runtimeScene, "40ffb5b2-0a88-4a4d-a494-74e722f50eee", gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("Score")));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GameOver", false);
}}

}


};gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDBallObjects1Objects = Hashtable.newFrom({"Ball": gdjs.GamePlayCode.GDBallObjects1});
gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDJumpObjects1Objects = Hashtable.newFrom({"Jump": gdjs.GamePlayCode.GDJumpObjects1});
gdjs.GamePlayCode.asyncCallback9808516 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("Ball"), gdjs.GamePlayCode.GDBallObjects3);

{for(var i = 0, len = gdjs.GamePlayCode.GDBallObjects3.length ;i < len;++i) {
    gdjs.GamePlayCode.GDBallObjects3[i].getBehavior("Physics2").setGravityScale(3.8);
}
}{runtimeScene.getScene().getVariables().get("CanJump").setNumber(0);
}}
gdjs.GamePlayCode.eventsList7 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.GamePlayCode.GDBallObjects2) asyncObjectsList.addObject("Ball", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.03), (runtimeScene) => (gdjs.GamePlayCode.asyncCallback9808516(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.GamePlayCode.asyncCallback9808196 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("Ball"), gdjs.GamePlayCode.GDBallObjects2);

{for(var i = 0, len = gdjs.GamePlayCode.GDBallObjects2.length ;i < len;++i) {
    gdjs.GamePlayCode.GDBallObjects2[i].clearForces();
}
}{for(var i = 0, len = gdjs.GamePlayCode.GDBallObjects2.length ;i < len;++i) {
    gdjs.GamePlayCode.GDBallObjects2[i].addForce(2300, 0, 1);
}
}
{ //Subevents
gdjs.GamePlayCode.eventsList7(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.GamePlayCode.eventsList8 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
for (const obj of gdjs.GamePlayCode.GDBallObjects1) asyncObjectsList.addObject("Ball", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.5), (runtimeScene) => (gdjs.GamePlayCode.asyncCallback9808196(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.GamePlayCode.eventsList9 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Ball"), gdjs.GamePlayCode.GDBallObjects1);
gdjs.copyArray(runtimeScene.getObjects("Jump"), gdjs.GamePlayCode.GDJumpObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
isConditionTrue_1 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDBallObjects1Objects, gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDJumpObjects1Objects, false, runtimeScene, false);
if (isConditionTrue_1) {
isConditionTrue_1 = false;
isConditionTrue_1 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("CanJump")) == 1;
}
isConditionTrue_0 = isConditionTrue_1;
}
if (isConditionTrue_0) {
/* Reuse gdjs.GamePlayCode.GDBallObjects1 */
{for(var i = 0, len = gdjs.GamePlayCode.GDBallObjects1.length ;i < len;++i) {
    gdjs.GamePlayCode.GDBallObjects1[i].addForce(0, -(1000), 1);
}
}{for(var i = 0, len = gdjs.GamePlayCode.GDBallObjects1.length ;i < len;++i) {
    gdjs.GamePlayCode.GDBallObjects1[i].getBehavior("Physics2").setGravityScale(2);
}
}
{ //Subevents
gdjs.GamePlayCode.eventsList8(runtimeScene);} //End of subevents
}

}


};gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDmoveGroundObjects2Objects = Hashtable.newFrom({"moveGround": gdjs.GamePlayCode.GDmoveGroundObjects2});
gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDmoveGroundObjects2Objects = Hashtable.newFrom({"moveGround": gdjs.GamePlayCode.GDmoveGroundObjects2});
gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDJumpObjects2Objects = Hashtable.newFrom({"Jump": gdjs.GamePlayCode.GDJumpObjects2});
gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDJumpObjects1Objects = Hashtable.newFrom({"Jump": gdjs.GamePlayCode.GDJumpObjects1});
gdjs.GamePlayCode.eventsList10 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("groundSpawn"), gdjs.GamePlayCode.GDgroundSpawnObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.GamePlayCode.GDgroundSpawnObjects2.length;i<l;++i) {
    if ( gdjs.GamePlayCode.GDgroundSpawnObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.GamePlayCode.GDgroundSpawnObjects2[k] = gdjs.GamePlayCode.GDgroundSpawnObjects2[i];
        ++k;
    }
}
gdjs.GamePlayCode.GDgroundSpawnObjects2.length = k;
if (isConditionTrue_0) {
gdjs.GamePlayCode.GDmoveGroundObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDmoveGroundObjects2Objects, gdjs.evtTools.input.getCursorX(runtimeScene, "", 0) - 1000, gdjs.evtTools.input.getCursorY(runtimeScene, "", 0), "");
}{for(var i = 0, len = gdjs.GamePlayCode.GDmoveGroundObjects2.length ;i < len;++i) {
    gdjs.GamePlayCode.GDmoveGroundObjects2[i].activateBehavior("Physics2", false);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("moveGround"), gdjs.GamePlayCode.GDmoveGroundObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDmoveGroundObjects2Objects, runtimeScene, true, false);
if (isConditionTrue_1) {
isConditionTrue_1 = false;
isConditionTrue_1 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Right");
}
isConditionTrue_0 = isConditionTrue_1;
}
if (isConditionTrue_0) {
/* Reuse gdjs.GamePlayCode.GDmoveGroundObjects2 */
{for(var i = 0, len = gdjs.GamePlayCode.GDmoveGroundObjects2.length ;i < len;++i) {
    gdjs.GamePlayCode.GDmoveGroundObjects2[i].activateBehavior("Physics2", true);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("jumpSpawn"), gdjs.GamePlayCode.GDjumpSpawnObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.GamePlayCode.GDjumpSpawnObjects2.length;i<l;++i) {
    if ( gdjs.GamePlayCode.GDjumpSpawnObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.GamePlayCode.GDjumpSpawnObjects2[k] = gdjs.GamePlayCode.GDjumpSpawnObjects2[i];
        ++k;
    }
}
gdjs.GamePlayCode.GDjumpSpawnObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9811972);
}
}
if (isConditionTrue_0) {
gdjs.GamePlayCode.GDJumpObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDJumpObjects2Objects, gdjs.evtTools.input.getCursorX(runtimeScene, "", 0) - 1000, gdjs.evtTools.input.getCursorY(runtimeScene, "", 0), "");
}{for(var i = 0, len = gdjs.GamePlayCode.GDJumpObjects2.length ;i < len;++i) {
    gdjs.GamePlayCode.GDJumpObjects2[i].setWidth(529);
}
}{for(var i = 0, len = gdjs.GamePlayCode.GDJumpObjects2.length ;i < len;++i) {
    gdjs.GamePlayCode.GDJumpObjects2[i].setHeight(275);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Jump"), gdjs.GamePlayCode.GDJumpObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDJumpObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_1) {
isConditionTrue_1 = false;
isConditionTrue_1 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Right");
}
isConditionTrue_0 = isConditionTrue_1;
}
if (isConditionTrue_0) {
/* Reuse gdjs.GamePlayCode.GDJumpObjects1 */
{runtimeScene.getScene().getVariables().get("CanJump").setNumber(1);
}{for(var i = 0, len = gdjs.GamePlayCode.GDJumpObjects1.length ;i < len;++i) {
    gdjs.GamePlayCode.GDJumpObjects1[i].activateBehavior("Draggable", false);
}
}}

}


};gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDSpikeObjects2Objects = Hashtable.newFrom({"Spike": gdjs.GamePlayCode.GDSpikeObjects2});
gdjs.GamePlayCode.asyncCallback9814852 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(runtimeScene.getObjects("Ball"), gdjs.GamePlayCode.GDBallObjects2);
gdjs.GamePlayCode.GDSpikeObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDSpikeObjects2Objects, (( gdjs.GamePlayCode.GDBallObjects2.length === 0 ) ? 0 :gdjs.GamePlayCode.GDBallObjects2[0].getPointX("")) + 6500, (( gdjs.GamePlayCode.GDBallObjects2.length === 0 ) ? 0 :gdjs.GamePlayCode.GDBallObjects2[0].getPointY("")), "");
}{runtimeScene.getScene().getVariables().get("Spike").setNumber(1);
}}
gdjs.GamePlayCode.eventsList11 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(gdjs.randomInRange(30, 50)), (runtimeScene) => (gdjs.GamePlayCode.asyncCallback9814852(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.GamePlayCode.eventsList12 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("Spike")) == 1;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("Spike").setNumber(0);
}
{ //Subevents
gdjs.GamePlayCode.eventsList11(runtimeScene);} //End of subevents
}

}


};gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDStarObjects3Objects = Hashtable.newFrom({"Star": gdjs.GamePlayCode.GDStarObjects3});
gdjs.GamePlayCode.asyncCallback9815748 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(runtimeScene.getObjects("Ball"), gdjs.GamePlayCode.GDBallObjects3);
gdjs.GamePlayCode.GDStarObjects3.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDStarObjects3Objects, (( gdjs.GamePlayCode.GDBallObjects3.length === 0 ) ? 0 :gdjs.GamePlayCode.GDBallObjects3[0].getPointX("")) + 6500, (( gdjs.GamePlayCode.GDBallObjects3.length === 0 ) ? 0 :gdjs.GamePlayCode.GDBallObjects3[0].getPointY("")) + gdjs.randomFloatInRange(1000, -(1000)), "");
}{runtimeScene.getScene().getVariables().get("Star").setNumber(1);
}}
gdjs.GamePlayCode.eventsList13 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(gdjs.randomInRange(30, 50)), (runtimeScene) => (gdjs.GamePlayCode.asyncCallback9815748(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDBallObjects1Objects = Hashtable.newFrom({"Ball": gdjs.GamePlayCode.GDBallObjects1});
gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDStarObjects1Objects = Hashtable.newFrom({"Star": gdjs.GamePlayCode.GDStarObjects1});
gdjs.GamePlayCode.eventsList14 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("Star")) == 1;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("Star").setNumber(0);
}
{ //Subevents
gdjs.GamePlayCode.eventsList13(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Ball"), gdjs.GamePlayCode.GDBallObjects1);
gdjs.copyArray(runtimeScene.getObjects("Star"), gdjs.GamePlayCode.GDStarObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDBallObjects1Objects, gdjs.GamePlayCode.mapOfGDgdjs_46GamePlayCode_46GDStarObjects1Objects, false, runtimeScene, false);
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("Score").add(5);
}}

}


};gdjs.GamePlayCode.eventsList15 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Ball"), gdjs.GamePlayCode.GDBallObjects1);
gdjs.copyArray(runtimeScene.getObjects("Jump"), gdjs.GamePlayCode.GDJumpObjects1);
gdjs.copyArray(runtimeScene.getObjects("moveGround"), gdjs.GamePlayCode.GDmoveGroundObjects1);
{gdjs.playerAuthentication.openAuthenticationWindow(runtimeScene);
}{gdjs.evtTools.camera.setCameraZoom(runtimeScene, 0.2, "", 0);
}{for(var i = 0, len = gdjs.GamePlayCode.GDBallObjects1.length ;i < len;++i) {
    gdjs.GamePlayCode.GDBallObjects1[i].addForce(2300, 0, 1);
}
}{for(var i = 0, len = gdjs.GamePlayCode.GDmoveGroundObjects1.length ;i < len;++i) {
    gdjs.GamePlayCode.GDmoveGroundObjects1[i].activateBehavior("Physics2", false);
}
}{runtimeScene.getScene().getVariables().get("CanJump").setNumber(0);
}{for(var i = 0, len = gdjs.GamePlayCode.GDJumpObjects1.length ;i < len;++i) {
    gdjs.GamePlayCode.GDJumpObjects1[i].activateBehavior("Draggable", true);
}
}{runtimeScene.getScene().getVariables().get("Spike").setNumber(1);
}{gdjs.evtTools.sound.playMusic(runtimeScene, "5325981204873216.wav", true, 100, 1);
}{runtimeScene.getScene().getVariables().get("Star").setNumber(1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("BlackSquareDecoratedButton"), gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects1.length;i<l;++i) {
    if ( gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects1[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects1[k] = gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects1[i];
        ++k;
    }
}
gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects1.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("CountDown"), gdjs.GamePlayCode.GDCountDownObjects1);
{for(var i = 0, len = gdjs.GamePlayCode.GDCountDownObjects1.length ;i < len;++i) {
    gdjs.GamePlayCode.GDCountDownObjects1[i].hide(false);
}
}
{ //Subevents
gdjs.GamePlayCode.eventsList4(runtimeScene);} //End of subevents
}

}


{


gdjs.GamePlayCode.eventsList5(runtimeScene);
}


{


gdjs.GamePlayCode.eventsList6(runtimeScene);
}


{


gdjs.GamePlayCode.eventsList9(runtimeScene);
}


{


gdjs.GamePlayCode.eventsList10(runtimeScene);
}


{


gdjs.GamePlayCode.eventsList12(runtimeScene);
}


{


gdjs.GamePlayCode.eventsList14(runtimeScene);
}


};

gdjs.GamePlayCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.GamePlayCode.GDBallObjects1.length = 0;
gdjs.GamePlayCode.GDBallObjects2.length = 0;
gdjs.GamePlayCode.GDBallObjects3.length = 0;
gdjs.GamePlayCode.GDBallObjects4.length = 0;
gdjs.GamePlayCode.GDBallObjects5.length = 0;
gdjs.GamePlayCode.GDBallObjects6.length = 0;
gdjs.GamePlayCode.GDGroundObjects1.length = 0;
gdjs.GamePlayCode.GDGroundObjects2.length = 0;
gdjs.GamePlayCode.GDGroundObjects3.length = 0;
gdjs.GamePlayCode.GDGroundObjects4.length = 0;
gdjs.GamePlayCode.GDGroundObjects5.length = 0;
gdjs.GamePlayCode.GDGroundObjects6.length = 0;
gdjs.GamePlayCode.GDmoveGroundObjects1.length = 0;
gdjs.GamePlayCode.GDmoveGroundObjects2.length = 0;
gdjs.GamePlayCode.GDmoveGroundObjects3.length = 0;
gdjs.GamePlayCode.GDmoveGroundObjects4.length = 0;
gdjs.GamePlayCode.GDmoveGroundObjects5.length = 0;
gdjs.GamePlayCode.GDmoveGroundObjects6.length = 0;
gdjs.GamePlayCode.GDgameOverObjects1.length = 0;
gdjs.GamePlayCode.GDgameOverObjects2.length = 0;
gdjs.GamePlayCode.GDgameOverObjects3.length = 0;
gdjs.GamePlayCode.GDgameOverObjects4.length = 0;
gdjs.GamePlayCode.GDgameOverObjects5.length = 0;
gdjs.GamePlayCode.GDgameOverObjects6.length = 0;
gdjs.GamePlayCode.GDUIObjects1.length = 0;
gdjs.GamePlayCode.GDUIObjects2.length = 0;
gdjs.GamePlayCode.GDUIObjects3.length = 0;
gdjs.GamePlayCode.GDUIObjects4.length = 0;
gdjs.GamePlayCode.GDUIObjects5.length = 0;
gdjs.GamePlayCode.GDUIObjects6.length = 0;
gdjs.GamePlayCode.GDgroundSpawnObjects1.length = 0;
gdjs.GamePlayCode.GDgroundSpawnObjects2.length = 0;
gdjs.GamePlayCode.GDgroundSpawnObjects3.length = 0;
gdjs.GamePlayCode.GDgroundSpawnObjects4.length = 0;
gdjs.GamePlayCode.GDgroundSpawnObjects5.length = 0;
gdjs.GamePlayCode.GDgroundSpawnObjects6.length = 0;
gdjs.GamePlayCode.GDjumpSpawnObjects1.length = 0;
gdjs.GamePlayCode.GDjumpSpawnObjects2.length = 0;
gdjs.GamePlayCode.GDjumpSpawnObjects3.length = 0;
gdjs.GamePlayCode.GDjumpSpawnObjects4.length = 0;
gdjs.GamePlayCode.GDjumpSpawnObjects5.length = 0;
gdjs.GamePlayCode.GDjumpSpawnObjects6.length = 0;
gdjs.GamePlayCode.GDJumpObjects1.length = 0;
gdjs.GamePlayCode.GDJumpObjects2.length = 0;
gdjs.GamePlayCode.GDJumpObjects3.length = 0;
gdjs.GamePlayCode.GDJumpObjects4.length = 0;
gdjs.GamePlayCode.GDJumpObjects5.length = 0;
gdjs.GamePlayCode.GDJumpObjects6.length = 0;
gdjs.GamePlayCode.GDSpikeObjects1.length = 0;
gdjs.GamePlayCode.GDSpikeObjects2.length = 0;
gdjs.GamePlayCode.GDSpikeObjects3.length = 0;
gdjs.GamePlayCode.GDSpikeObjects4.length = 0;
gdjs.GamePlayCode.GDSpikeObjects5.length = 0;
gdjs.GamePlayCode.GDSpikeObjects6.length = 0;
gdjs.GamePlayCode.GDBarrierObjects1.length = 0;
gdjs.GamePlayCode.GDBarrierObjects2.length = 0;
gdjs.GamePlayCode.GDBarrierObjects3.length = 0;
gdjs.GamePlayCode.GDBarrierObjects4.length = 0;
gdjs.GamePlayCode.GDBarrierObjects5.length = 0;
gdjs.GamePlayCode.GDBarrierObjects6.length = 0;
gdjs.GamePlayCode.GDCountDownObjects1.length = 0;
gdjs.GamePlayCode.GDCountDownObjects2.length = 0;
gdjs.GamePlayCode.GDCountDownObjects3.length = 0;
gdjs.GamePlayCode.GDCountDownObjects4.length = 0;
gdjs.GamePlayCode.GDCountDownObjects5.length = 0;
gdjs.GamePlayCode.GDCountDownObjects6.length = 0;
gdjs.GamePlayCode.GDScoreObjects1.length = 0;
gdjs.GamePlayCode.GDScoreObjects2.length = 0;
gdjs.GamePlayCode.GDScoreObjects3.length = 0;
gdjs.GamePlayCode.GDScoreObjects4.length = 0;
gdjs.GamePlayCode.GDScoreObjects5.length = 0;
gdjs.GamePlayCode.GDScoreObjects6.length = 0;
gdjs.GamePlayCode.GDStarObjects1.length = 0;
gdjs.GamePlayCode.GDStarObjects2.length = 0;
gdjs.GamePlayCode.GDStarObjects3.length = 0;
gdjs.GamePlayCode.GDStarObjects4.length = 0;
gdjs.GamePlayCode.GDStarObjects5.length = 0;
gdjs.GamePlayCode.GDStarObjects6.length = 0;
gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects1.length = 0;
gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects2.length = 0;
gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects3.length = 0;
gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects4.length = 0;
gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects5.length = 0;
gdjs.GamePlayCode.GDBlackSquareDecoratedButtonObjects6.length = 0;

gdjs.GamePlayCode.eventsList15(runtimeScene);

return;

}

gdjs['GamePlayCode'] = gdjs.GamePlayCode;
